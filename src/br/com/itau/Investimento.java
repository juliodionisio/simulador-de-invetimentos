package br.com.itau;

public class Investimento {

    private double valor;
    private int quantidadeMeses;

    public Investimento(double valor, int quantidadeMeses){
        this.valor = valor;
        this.quantidadeMeses = quantidadeMeses;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }
}
