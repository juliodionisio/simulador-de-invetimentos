package br.com.itau;

public class Calculadora {
    private Investimento investimento;

    public Calculadora(Investimento investimento){
        this.investimento = investimento;
    }

    public double jurosPeriodo(double juros) {

        return (investimento.getValor()*Math.pow(((juros/100)+1), investimento.getQuantidadeMeses()));
    }
}
