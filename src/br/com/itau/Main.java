package br.com.itau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner= new Scanner(System.in);

        System.out.println("Qual valor você deseja investir?");

        double valorInvestido = scanner.nextDouble();

        System.out.println("Por quantos meses?");

        int quantidadeDeMeses = scanner.nextInt();

        Investimento investimento = new Investimento(valorInvestido, quantidadeDeMeses);
        Calculadora calculadora = new Calculadora(investimento);

        //System.out.println("Qual a taxa desejada? (Caso não seja informado, será considerado 0,7% a.m");

        //double juros = scanner.nextDouble();
        double juros = 0.7;


        System.out.println("O valor no final do período será: " + calculadora.jurosPeriodo(juros));


    }
}
